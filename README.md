![img-pfile](https://bitbucket.org/DiamondSparrow/cv/raw/83ae3c9df9a860ebea83b2d04e81c4a3bbbbbfe7/img/profile.jpeg)
# **DEIMANTAS ŽVIRBLIS**

*Embedded Software Developer/Archtect*, *Vilnius, Lithuania*

*deimantas.zvirblis@gmail.com*, [www.linkedin.com/in/deimantas-žvirblis](http://www.linkedin.com/in/deimantas-žvirblis-b1830645)


## Summary
---
Embedded Software Engineer/Developer with more than 8 years experience with a demonstrated history of working in the consumer electronics industry. Skilled in Embedded Software, C/C++, Electronics design. Strong engineering professional with a Master focused in Automation from Vilnius Gediminas Technical University. 

My career as embedded software engineer started at UAB Aedilis, where i was not only learning but also developing Modbus-RTU devices and platform Cloud Industries.

After 3 years i moved to Eldes, where continued my career as embedded software engineer. During work at Eldes i was responsible for developing testing equipment (hardware, firmware and software) for production. After a year moved to developing main Eldes products. Also had a possibility to put my self to a test like a project manager, which gave me good planning skills for future.
Now my role at Eldes not only covers embedded software development but also as architecture designer of Eldes security solutions.

During my 8 years career i had several side projects.
Firstly became an "astronaut", not real one (always wanted to be), but the one who develops nano-satellite LituanicaSAT-2 at NanoAvionics. This was a year project for me which extended my experiences all the way to the space. 
Of course there was more projects, like Luminaire - street led lights controller, Windows Changer - BLE car windows controller and my hobby project DS-2 robot and many more interesting projects to research and develop.

In every engineer there is a inner child who pushes them forward, so in me.
 
 
 
## Experience
---
![img-0](https://bitbucket.org/DiamondSparrow/cv/raw/78404e8cd0970a1d2e7e2679f4d70db815133e8e/img/0.png) 
***Embedded Software Developer and Architect at ELDES, UAB***

*July 2013 – Present (5 years 4 months)*

*Vilnius, Lithuania*

Eldes is company that develops wireless home security solutions.

My work in this company started from developing production testing tools from hardware, firmware to software.
After around a year moved to developing main new products like ESR100 (linux based computer), smart auto tracker Monimoto, hybrid alarm control panel ESIM384 and standalone alarm control panel Pitbull-PRO.
Also in my career at this company took the project manager role, which I did not liked very much because of paperwork, but it gave me good planning skills.

Now my role is not only as a developer but also as embedded software architect, technical lead.


![img-1](https://bitbucket.org/DiamondSparrow/cv/raw/78404e8cd0970a1d2e7e2679f4d70db815133e8e/img/1.jpeg)
***Embedded Software Engineer at NanoAvionics, JSC*** 

*March 2015 – March 2016 (1 year 1 month)*

*Vilnius, Lithuania*

Working with NanoAvionics was all about the mission QB50. We were developing new 3 units CubeSat called LituanicaSAT-2. It was their second small satellite.

The main unique thing about this missions and the CubeSat itself that it was equipped with a propulsion engine which in the future was successfully working in the space, around the earth.

I was responsible for communication between different satellite units, data storing and main on board computer developing. Participating in such project gave me deeper understanding about space and technologies used to get there.

Whats more, i have deepened my knowledge in STM3F0/F4/F4 MCU's, CAN interface, MEM's sensors and communication where several microcontrollers must work in one system for one purpose.
 

![img-2](https://bitbucket.org/DiamondSparrow/cv/raw/78404e8cd0970a1d2e7e2679f4d70db815133e8e/img/2.png)
***Embedded Software Engineer at Aedilis, UAB***

*August 2010 – July 2013 (3 years)*

*Vilnius, Lithuania*

Aedilis was my first work experience as embedded software engineer.

In 3 years working at this company i was developing Modbus RTU controllers used for controlling automation devices and getting information from them. 
The biggest challenge at this company was in developing a Cloud solution for industries - cloudindustries.eu. I was responsible for developing Linux based computer which gathers data from various devices (Power meters, Automation devices, renewable power plants) in various protocols (Modbus-RTU, specific vendors protocols) and send that data to Cloud Servers. This solution was able to not only provide real time data to Cloud but also collect data, when there is no internet.

 

## Education
---
* ***Vilnius Gediminas Technical University***

*Master*, *Automation*, *2011 – 2013*

This Master thesis was a research of the robot inter – orientation system, when the
direction is determined by the magnetic field effect on the robot.

* ***Vilnius Gediminas Technical University***

*Bachelor*, *Automation and control*, *2007 – 2011*

Bachelor thesis: Robot Module Control

 

## Projects
---
* ***Windows Changer***

*Starting September 2018*

Windows changer is a small controller for cars used to quickly open/close car windows.
This controller is based on Nordic Semiconductors Bluetooth low energy chip nRF52.
Device is BLE peripheral device with Device information, Device Firmware Upgrade services and custom 128-bit UUID service used for controlling car windows.

My responsibility is to develop embedded software for a controller. 

Role: Embedded Software Engineer.

* ***Luminaire***

*April 2017 – April 2018*

Luminaire is a controller designed for controlling led lights in streets.
This controllers stores statistic data of how long it was turned on. By analyzing collected data and adapting midnight algorithm, controller calculates what time is it. Depending on time of the day it chooses best light intensity to set. Also controller was able to control led color temperature depending on time of the day.

Additionally, this controller had DALI interface, through which it can get commands and act as slave in DALI network.
My responsibility in this project was not only to program controller firmware, but also to write configuration software for Windows using Visual Studio and C#.

This project was good opportunity for me to participate in different fields of industry, like LED controllers and gather more experience about it.

Role: Software and Embedded Software Developer.

* ***LituanicaSAT-2***

*January 2014 – January 2014*

LituanicaSAT-2 is 3U CubeSat in-orbit technology demonstration mission led by Vilnius University. The mission is a part of a network of 50 nano-satellites called "QB50" that will be launched together in the first half of 2016.
The science objective of the mission is to carry out long term measurements of key parameters and constituents in yet largely unexplored lower thermosphere and ionosphere. The satellites will be deployed into a circular polar orbit at an altitude higher than 300 km. NanoAvionics JSC has been contracted by Vilnius University to build a satellite platform and a propulsion system for LituanicaSAT-2. The propulsion system is green mono-propellant micro-thruster able to perform high impulse orbital maneuvers and drag compensation capabilities for a small scale satellite. The system is designed to provide 0.3N thrust and up to 200 m/s of delta V. It is powerful enough to perform impulsive Hohman orbital transfer, orbit shape corrections or even change of inclination for a 3kg satellite. The fuel used is a green monopropellant fuel blend based on ADN.

Role: embedded software engineer

* ***DS-2***

This project has no end or start time, this is my hobby project which came from passion to robots.
My first robot - DS-1 was my project during my studies in VGTU which helped me to become embedded software engineer.
DS-2 is a second generation which is linux based robot. The main unit of this robot is BeagleBone Black to which is connected several motor controllers, GPS receiver, WiFi dongle. DS-2 is controlled by Xbox controller which is connected to a laptop and connects to DS-2 through WiFi, control vector is sent to a robot where to move.
Now this project is like my research lab, where i can connect different modules to learn how they work and how they can be programmed. These modules are: gyroscopes, accelerometers, magnetometers, OLED displays, ultrasonic sensors, RF transceivers and so on.

Some links to to robot videos:

* [https://www.youtube.com/watch?v=ZnBvK8-LVuw](https://www.youtube.com/watch?v=ZnBvK8-LVuw)
* [https://www.youtube.com/watch?v=O-YGDP9ktWM](https://www.youtube.com/watch?v=O-YGDP9ktWM)

Role: Creator, Embedded Software Engineer.
 
 
 
## Skills
---
*Software:*

* Keil
* Eclipse
* MS Visual Studio
* Altium Designer
* Git (GitLab, GitHub, BitBucket)
* Redmine

*Programming Languages:*

* C/C++
* C#
* MySQL

*Scripting Languages:*

* Bash, Shell

*Microcontrollers:*

* NXP: LPC8xx, LPC11xx, LPC15xx, LPC17xx, LPC18xx.
* ST: STM32F0/F3/F4.
* Nordic Semicondoctors: nRF51, nRF52.
* Atmel AVR: ATmega8, ATmega16, ATmega168, ATmega128, ATmega256.

*Microcontrollers peripherals:*

* ADC
* CAN
* DAC
* DMA
* I2C
* SPI
* UART

*Operating Systems:*

* MS Windows.
* Linux.
* Keil RTX. 
* Free RTOS.

*Project management methodologies:*

* Scrum (Agile).
* LEAN.
* Waterfall.

Other:

* Ublox GSM 2G/3G modems, AT Commands interface.
* Ublox GPS module, NMEA, UBX.
* Bluetooth Low Energy 4.1, 4.1, 4.2.
* Modbus-RTU.
* cJSON.

## Additional information
---
*Languages:*

* Lithuanian - mother toounge;
* English - good.
* Russian - basic.

